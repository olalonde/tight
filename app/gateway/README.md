A web socket server is a REST API implementing at least one
web socket endpoint.

Internal API methods (must be only authorized for internal system),
no external users.

GET /cons lists connections
POST /cons/:con_id/events Forward event to connection through websocket

This basically allows sending message to identified connection through
an HTTP API.

External API. Optionally authenticated.

GET / create websocket connection
