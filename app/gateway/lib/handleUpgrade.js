var http = require('http');

module.exports = function (wss, app) {

  // https://github.com/websockets/ws/blob/master/lib/WebSocketServer.js#L77
  return function (req, socket, upgradeHead) {
    /*
    socket._writeOrig = socket.write;
    socket.write = function () {
      var args = Array.prototype.slice.call(arguments);
      console.log(args); //debugging
      return socket._writeOrig.apply(socket, args);
    };
    */
    var res = new http.ServerResponse(req);
    res.assignSocket(socket);

    res.on('finish', function () {
      res.socket.destroy();
    });

    res.websocket = function (cb) {
      var head = new Buffer(upgradeHead.length);
      upgradeHead.copy(head);
      wss.handleUpgrade(req, socket, head, function (client) {
        //client.req = req; res.req
        wss.emit('connection'+req.url, client);
        wss.emit('connection', client);
        cb(client);
      });
    };

    return app(req, res);
  };
};
