#!/usr/bin/env node

var config  = require('../config');
var gateway = require('./')(config);

gateway.start();
