var express    = require('express');
var stream     = require('stream');
var errors     = require('common-errors');
var bodyParser = require('body-parser');
var agent      = require('superagent');
var WebSocket  = require('ws');

function router() {
  return express.Router({
    mergeParams: true // inherit parent router's params
  });
}

function auth(req, res, next) {
  var key = req.headers['x-api-key'];
  if (key && key === req.app.get('config').apikey) {
    req.isRoot = true;
  }
  next();
}

function mustBeRoot(req, res, next) {
  if (req.isRoot) return next();
  next(new errors.AuthenticationRequiredError());
}

module.exports = function (app) {
  var config = app.get('config');
  var sockets = app.get('sockets');
  var wss = app.get('wss');

  var readyStates = [ 'connecting', 'open', 'closing', 'closed' ];

  var wsRouter = router()
    .get('/', mustBeRoot, function (req, res, next) {
      res.json(Object.keys(sockets).reduce(function (acc, key) {
        var socket = sockets[key];
        var ws = socket.ws;
        acc[key] = {
          url: socket.url
        };
        if (ws) {
          acc[key].readyState = readyStates[ws.readyState];
        }
        else {
          acc[key].readyState = 'waiting';
        }
        return acc;
      }, {}));
    })
    // Forward messages to websocket and/or create websocket
    .post('/:socketId', mustBeRoot, function (req, res, next) {
      // "Virtual" stream that buffers data until Ws exists
      var socketId = req.params.socketId;
      var ttl = req.query.ttl || config.gateway.ws.ttl;
      var socket = sockets[socketId];
      if (!socket) {
        socket = {
          buffer: [],
          send: function (msg) {
            if (this.ws) {
              var msgString = JSON.stringify(msg) + '\n';
              return this.ws.send(msgString);
            }
            this.buffer.push(msg);
          },
          expireTimer: setTimeout(function () {
              // No one connected to the socket within ttl
              if (!socket.ws) {
                // Delete it
                socket.destroy();
              }
            }, ttl),
          socketId: socketId,
          url: config.gateway.url.format() + '/' + socketId,
          destroy: function () {
            var self = this;
            var url = config.server.url.format();
            url += '/websockets/';
            url += self.url;
            // Delete websocket connection from server
            agent.del(url)
              .set('X-Api-Key', config.apikey)
              .set('Content-Type', 'application/json')
              .set('Accept', 'application/json')
              .end(function (err, res) {
                if (err) {
                  // TODO: ws.send({ ok:false, ... })
                  return console.error(err);
                }
                //console.log(res);
              });
            delete sockets[socketId];
          },
          assignWebsocket: function (ws) {
            var self = this;
            this.ws = ws;
            ws.on('close', function () {
              self.destroy();
            });
            ws.on('message', function (message) {
              try {
                message = JSON.parse(message);
              }
              catch (e) {}

              // Respond to pings without round-trip
              // to server
              if (message.type === 'ping') {
                var response = {
                  type: 'pong',
                  reply_to: message.id
                };
                // Echo received properties (except non-scalars)
                Object.keys(message).forEach(function (key) {
                  if (key === 'id') return;
                  if (!response[key] && typeof message[key] !== 'object') {
                    response[key] = message[key];
                  }
                });
                /*
                console.log(message);
                console.log(response);
                */
                socket.send(response);
                return;
              }
              // Forward message to server
              var url = config.server.url.format();
              url += '/websockets/';
              url += self.url;

              // TODO: refactor into app.get('server')
              var a = agent.post(url)
                .set('X-Api-Key', config.apikey)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .send(message)
                .end(function (err, res) {
                  if (err) {
                    // TODO: ws.send({ ok:false, ... })
                    console.error('Error forwarding event to chat server.');
                    console.error(err);
                    return;
                  }
                  //console.log(res);
                });
            });
            this.buffer.forEach(function (msg) {
              self.send(msg);
            });
            delete this.buffer;
          }
        };
        sockets[req.params.socketId] = socket;
      }
      if (req.body) socket.send(req.body);
      res.json({ ok: true });
    })
    .use('/:socketId', function (req, res, next) {
      var socket = sockets[req.params.socketId];
      if (!socket) {
        return next(new errors.HttpStatusError(400, 'Socket does not exist.'));
      }
      if (socket.ws) {
        return next(new errors.HttpStatusError(400, 'Socket already in use.'));
      }
      res.websocket(function (ws) {
        socket.assignWebsocket(ws);
      });
    });

  // Authenticate cross service requests
  app.use(auth);
  app.use(bodyParser.json());

  // Mostly for testing
  var consRouter = router()
    .get('/', function (req, res, next) {
      var clients = wss.clients.map(function (client) {
        return client.upgradeReq.connection.remoteAddress;
      });
      res.json(clients);
    });

  app.use('/cons', mustBeRoot, consRouter);

  // Mostly for testing
  app.use('/ping', function (req, res, next) {
    // Test error
    if (req.query.error) {
      return next(new errors.NotFoundError('Websocket'));
    }
    return res.websocket(function (ws) {
      ws.send('pong');
      //ws.close();
    });
  });
  app.use('/', wsRouter);

};
