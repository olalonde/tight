var debug         = require('debug')('gateway');
var express       = require('express');
var http          = require('http');
var errors        = require('common-errors');
var ws            = require('websocket-stream');
var handleUpgrade = require('./lib/handleUpgrade');
var toURLObject   = require('../utils').toURLObject;
var _             = require('lodash');
var routes        = require('./routes');

module.exports = function (config) {
  config = _.merge({
    // default properties
  }, config);
  config.gateway.url = toURLObject(config.gateway.url);
  config.server.url = toURLObject(config.server.url);

  var app = express();
  var server = http.createServer(app);
  var wss = new ws.Server({ noServer: true });

  /*
  wss.on('connection', function connection(ws) {
    console.log('new connection');
    ws.on('message', function incoming(message) {
      console.log('received: %s', message);
    });

    ws.send('something');
  });
  */

  app.set('wss', wss);
  app.set('config', config);
  app.set('sockets', {});

  routes(app);

  app.use(function (err, req, res, next) {
    var httpErr = new errors.HttpStatusError(err, req);
    if (httpErr.statusCode === 500) {
      console.error(httpErr.name);
      console.error(httpErr.message);
      console.error(httpErr.stack);
    }
    res.status(httpErr.statusCode);
    res.json(httpErr);
  });

  server.on('upgrade', handleUpgrade(wss, app));

  return {
    start: function (cb) {
      server.listen(config.gateway.port, cb);
    },
    stop: function (cb) {
      wss.close();
      server.close(function (err) {
        cb(err);
      });
    }
  };
};
