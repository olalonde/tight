var expect    = require('chai').expect;
var WebSocket = require('ws');

var config    = require('../config');
var gateway   = require('../gateway/')(config);
var server    = require('../server/')(config);

/*
// Debug why the server won't close
var sockets = [];
server.server.on('connection', function (socket) {
  console.log('new connection');
  socket.on('data', function (data) {
    socket.data = socket.data || [];
    socket.data.push(data.toString());
  });
  sockets.push(socket);
});
*/

var _ = require('lodash');

var db = require('./helpers').db(server.knex, server.models);

var ola = require('../client/')(_.merge({}, config, {
  token: 'validtoken'
}));

var aaron = require('../client/')(_.merge({}, config, {
  token: 'aarontoken'
}));

describe('client', function () {
  this.timeout(5000);

  before(function (done) {
    db.preTest(function (err) {
      if (err) return done(err);
      gateway.start(function (err) {
        if (err) return done(err);
        server.start(done);
      });
    });
  });

  it('rtm.start', function (done) {
    ola.rtm.start(function (err, res) {
      if (err) return done(err);
      expect(res.body.url).to.string(config.gateway.url);
      done();
    });
  });

  it('rtm.start(true)', function (done) {
    ola.rtm.start(true, function (err, res) {
      if (err) return done(err);
      expect(res.body.url).to.string(config.gateway.url);
      expect(ola.ws).to.be.an.instanceOf(WebSocket);
      var ws = ola.ws;
      ws.on('msg', function (message) {
        expect(message).to.eql({ type: 'hello' });
        done();
      });
    });
  });

  aaron.msgs = [];
  it('aaron.rtm.start(true)', function (done) {
    aaron.rtm.start(true, function (err, res) {
      if (err) return done(err);
      expect(res.body.url).to.string(config.gateway.url);
      expect(aaron.ws).to.be.an.instanceOf(WebSocket);
      var ws = aaron.ws;
      ws.on('msg', function (message) {
        aaron.msgs.push(message);
      });
      done();
    });
  });


  it('send a message to lobby', function (done) {
    ola.ws.send({
      type: 'message',
      channel: 1,
      text: 'Hello world!'
    }, function (res) {
      expect(res.reply_to).to.equal(1);
      done();
    });
  });

  it('aaron should receive message', function (done) {
    //console.log(aaron.msgs);
    (function checkMessage () {
      var msg = aaron.msgs[aaron.msgs.length-1];
      console.log('last message: ');
      console.log(msg);
      if (msg.type === 'message') {
        expect(msg).to.have.property('type', 'message');
        expect(msg).to.have.property('channel', 1);
        expect(msg).to.have.property('user', 1);
        expect(msg).to.have.property('text', 'Hello world!');
        return done();
      }
      setTimeout(checkMessage, 500);
    })();
  });

  it('message should be in channel history', function (done) {
    aaron.channels.history({ channel: 1 }, function (err, history) {
      if (err) return done(err);
      expect(history.length).to.equal(1);
      var ev = history[0];
      expect(ev.channel).equal(1);
      expect(ev.text).equal('Hello world!');
      expect(ev.user).equal(1);
      done();
    });
  });

  it('should close websocket', function (done) {
    ola.ws.on('close', function () {
      done();
    });
    ola.ws.close();
  });

  it('should close aaron websocket', function (done) {
    aaron.ws.on('close', function () {
      done();
    });
    aaron.ws.close();
  });

  after(function (done) {
    gateway.stop(function (err) {
      if (err) return done(err);
      /*
      sockets.forEach(function (socket) {
        console.log(socket.data);
        console.log(socket.readyState);
        console.log(socket.destroyed);
      });
      process.exit();
      */
      server.stop(done);
    });
  });
});
