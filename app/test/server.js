var request     = require('supertest');
var expect      = require('chai').expect;

var helpers     = require('./helpers');
var serverInit  = require('../server');
var gatewayInit = require('../gateway');
var config      = require('../config');

describe('server', function () {
  var server  = serverInit(config);
  var gateway = gatewayInit(config);
  var db      = helpers.db(server.knex, server.models);
  var app     = server.app;
  var agent   = request.agent(app);

  // Reset database
  before(function () {
    return db.preTest();
  });

  // Start websocket server
  before(function (done) {
    return gateway.start(done);
  });

  it('should start', function (done) {
    server.start(done);
  });

  it('should stop', function (done) {
    server.stop(done);
  });

  it('GET /api/rtm.start?token=invalidtoken', function (done) {
    agent
      .get('/api/rtm.start')
      .query({ token: 'invalidtoken' })
      .expect('Content-Type', /application\/json/)
      .expect(401)
      .expect(/token/)
      .end(done);
  });

  var wsUrl;
  it('GET /api/rtm.start?token=validtoken', function (done) {
    agent
      .get('/api/rtm.start')
      .query({ token: 'validtoken' })
      .expect(200)
      .expect(function (res) {
        expect(res.body.ok).to.equal(true);
        expect(res.body.url).to.match(/ws:\/\/.+\/.*/);
        expect(res.body.self).to.have.property('id', 1);
        expect(res.body.self).to.have.property('name', 'olalonde');
        expect(res.body.users).to.be.an('array');
        expect(res.body.channels).to.be.an('array');
        wsUrl = res.body.url;
      })
      .end(done);
  });

  it('should have websocket in database', function (done) {
    server.models.Websocket.forge({ url: wsUrl }).fetch().then(function (ws) {
      expect(ws.get('url')).to.eql(wsUrl);
      done();
    }).catch(done);
  });

  // Simulate message from websocket
  it('POST /websockets/ws://.../id', function (done) {
    var message = {
      type: 'message',
      channel: 1,
      text: 'hello world!'
    };

    agent
      .post('/api/websockets/' + wsUrl)
      .set('X-Api-Key', config.apikey)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send(message)
      .expect(200)
      .expect(function (res) {
        expect(res.body).to.deep.equal({ ok: true });
      })
      .end(done);
  });

  // Simulate message from websocket
  it('channels.history', function (done) {
    agent
      .get('/api/channels.history')
      .query({
        channel: 1,
        token: 'validtoken'
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(200)
      .expect(function (res) {
        var history = res.body;
        expect(history.length).to.equal(1);
        expect(history[0].channel).to.equal(1);
        expect(history[0].type).to.equal('message');
        expect(history[0].text).to.equal('hello world!');
        expect(history[0].user).to.equal(1);
      })
      .end(done);
  });

  var testUserId;
  it('POST /api/users', function (done) {
    agent
      .post('/api/users')
      .set('X-Api-Key', config.apikey)
      .send({
        name: 'testuser'
      })
      .expect(200)
      .expect(function (res) {
        expect(res.body.user).to.have.property('id');
        expect(res.body.user).to.have.property('name', 'testuser');
        expect(res.body.user).to.have.property('token');
        testUserId = res.body.user.id;
        console.log(testUserId);
      })
      .end(done);
  });

  var testUser;
  it('should have testuser in database', function (done) {
    server.models.User.forge({ id: testUserId }).fetch().then(function (user) {
      expect(user.get('id')).to.eql(testUserId);
      testUser = user;
      done();
    }).catch(done);
  });

  it('testuser should have joined the general channels', function (done) {
    testUser.load([ 'channels' ])
      .then(function (user) {
        var channels = user.related('channels').models;
        expect(channels.length).to.equal(2);
        channels.forEach(function (channel) {
          expect(channel.get('is_general')).to.equal(true);
        });
        done();
      });
  });

  it('DELETE /api/users/3', function (done) {
    agent
      .del('/api/users/' + testUserId)
      .set('X-Api-Key', config.apikey)
      .expect(200)
      .expect(function (res) {
        expect(res.body.ok).to.equal(true);
      })
      .end(done);
  });

});
