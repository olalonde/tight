var config    = require('../config');
var gateway   = require('../gateway/')(config);
var supertest = require('supertest');
var WebSocket = require('ws');
var expect    = require('chai').expect;
var util      = require('util');
var _url      = require('url');

describe('gateway', function () {
  var host = _url.parse(config.gateway.url).host;

  describe('basic tests', function () {
    it('should start', function (done) {
      gateway.start(done);
    });

    /*
    var wsUrl = _url.parse(config.gateway.url.format());
    wsUrl.protocol = 'ws:';
    wsUrl = wsUrl.format();
    */

    var url = 'http://' + host;
    var wsUrl = 'ws://' + host + '/ping';

    var client;

    // TODO: connection still open on http server here...
    it('/ping?error=1 should not connect', function (done) {
      var ws = new WebSocket(wsUrl + '?error=1');
      ws.on('error', function (err) {
        //console.log(err);
        ws.close();
        done();
      });
    });

    it('should let ws client connect to ping/', function (done) {
      client = new WebSocket(wsUrl);
      //client.on('open', done);
      client.on('message', function (msg) {
        expect(msg).to.equal('pong');
        done();
      });
    });

    it('should list active connections', function (done) {
      supertest(url)
        .get('/cons')
        .set('X-Api-Key', config.apikey)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          //console.log(res.body);
          expect(res.body.length).to.equal(1);
          expect(typeof res.body[0]).to.equal('string');
          done();
        });
    });

    it('should disconnect', function (done) {
      client.on('close', function () {
        done();
      });
      client.close();
    });

    it('should no longer list connection', function (done) {
      supertest(url)
        .get('/cons')
        .set('X-Api-Key', config.apikey)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body.length).to.equal(0);
          done();
        });
    });

    it('should stop server', function (done) {
      gateway.stop(done);
    });
  });
  describe('advanced tests', function () {
    var url = 'http://' + host;
    var wsUrl = 'ws://' + host;
    var client;

    before(function (done) {
      gateway.start(done);
    });

    it('connect to ws://localhost:3001/doesntexist', function (done) {
      client = new WebSocket(wsUrl + '/somesocketid');
      client.on('open', function () {
        done(new Error('could connect to invalid endpoint'));
      });
      client.on('error', function (err) {
        //console.log(util.inspect(err, { showHidden: true }));
        done();
      });
    });

    // Create Websocket endpoint
    it('POST /somesocketid', function (done) {
      supertest(url)
        .post('/somesocketid')
        .set('X-Api-Key', config.apikey)
        .set('Accept', 'application/json')
        .send({ type: 'hello' })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          console.log(res.body);
          done();
        });
    });

    it('GET / (waiting connection)', function (done) {
      supertest(url)
        .get('/')
        .set('X-Api-Key', config.apikey)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.have.property('somesocketid');
          done();
        });
    });

    it('connect to ws://localhost:3001/somesocketid', function (done) {
      client = new WebSocket(wsUrl + '/somesocketid');
      client.on('message', function (msg) {
        expect(msg).to.equal('{"type":"hello"}\n');
        done();
      });
    });

    it('GET / (opened connection)', function (done) {
      supertest(url)
        .get('/')
        .set('X-Api-Key', config.apikey)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body.somesocketid.readyState).to.equal('open');
          done();
        });
    });

    it('disconnect from ws://localhost:3001/somesocketid', function (done) {
      client.on('close', function () {
        done();
      });
      client.close();
    });

    it('GET / (closed connection)', function (done) {
      supertest(url)
        .get('/')
        .set('X-Api-Key', config.apikey)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body.somesocketid).to.eql(undefined);
          done();
        });
    });

    // Create short lived Websocket endpoint (1s)
    it('POST /shortlived', function (done) {
      supertest(url)
        .post('/somesocketid?ttl=1000')
        .set('X-Api-Key', config.apikey)
        .set('Accept', 'application/json')
        .send({ type: 'hello' })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          console.log(res.body);
          done();
        });
    });

    it('wait 1.1s and connect to ws://.../shortlived should error', function (done) {
      setTimeout(function () {
        client = new WebSocket(wsUrl + '/somesocketid');
        client.on('error', function (msg) {
          done();
        });
      }, 1100);
    });

    after('should stop server', function (done) {
      gateway.stop(done);
    });
  });
});
