var BPromise = require('bluebird');
var seeds    = require('../seeds');

module.exports = function (knex, models) {
  return {
    clear: function () {
      // http://stackoverflow.com/questions/20100245/how-can-i-execute-array-of-promises-in-sequential-order

      // Drop table of all models
      var tables = [
        'knex_migrations',
        'websockets',
        'events',
        'members',
        'channels',
        'users'
      ];

      return BPromise.reduce(tables, function (_, table) {
        return knex.schema.dropTableIfExists(table);
      }, []);
    },
    migrate: function () {
      return knex.migrate.latest();
    },
    seed: function () {
      // TODO
      return seeds(models);
    },
    preTest: function (cb) {
      var self = this;
      return self.clear()
        .then(function () {
          return self.migrate();
        })
        .tap(console.log)
        .then(function () {
          return self.seed();
        })
        .nodeify(cb);
    }
  };

};
