var request     = require('supertest');
var expect      = require('chai').expect;
var helpers     = require('./helpers');
var serverInit  = require('../server');
var gatewayInit = require('../gateway');
var config      = require('../config');

describe('server', function () {
  var server = serverInit(config);
  var knex   = server.knex;
  var db     = helpers.db(server.knex, server.models);
  var models = server.models;
  var bookshelf = server.bookshelf;

  var User = models.User;

  // Reset database
  before(function () {
    return db.preTest();
  });

  describe('Bookshelf model event', function () {
    it('should not create transaction', function () {
      var Model = bookshelf.Model.extend({
        tableName: 'users'
      });
      var testUser = Model.forge({ name: 'testuser' });
      testUser.on('created', function (model, resp, options) {
        expect(options).to.not.have.property('transacting');
      });
      return testUser.save();
    });
  });

  describe('User', function () {
    describe('create', function () {
      var user;
      before(function () {
        return User.forge({
            name: 'test'
          })
          .save()
          .then(function (model) {
            user = model;
            expect(user.get('name')).to.equal('test');
            expect(user.get('token')).to.have.length.above(10);
          });
      });

      it('should have joined general channels', function () {
        return user.load([ 'channels' ])
          .then(function () {
            user.related('channels').forEach(function (channel) {
              expect(channel.get('is_general')).to.equal(true);
            });
          });
      });
    });
  });

});
