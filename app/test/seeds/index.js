var BPromise = require('bluebird');

module.exports = function (models) {
  var User = models.User;
  var Channel = models.Channel;

  // Users
  var users = [
    {
      name: 'olalonde',
      token: 'validtoken'
    },
    {
      name: 'aaron',
      token: 'aarontoken'
    }
  ];

  // Remember, there is already a lobby channel
  // created by the knex migration.
  // Also, users auto join general channel
  // on creating.

  // Add one more general channel
  var channels = [
    {
      name: 'hangout',
      is_general: true
    }
  ];

  return BPromise.reduce(channels, function (_, channel) {
      return Channel.forge(channel).save();
    }, null)
    .then(function () {
      return BPromise.reduce(users, function (_, user) {
          return User.forge(user).save();
        }, null);
    })
    .then(function () {
    });
};
