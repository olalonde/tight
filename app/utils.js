var url = require('url');

function toURLObject (u) {
  if (typeof u === 'string') {
    u = url.parse(u);
  }
  return u;
}

module.exports = {
  toURLObject: toURLObject
};
