exports.up = function (knex, BPromise) {
  return knex.schema.createTable('events', function (t) {
    t.increments('id').primary();
    t.string('type', 200).index();
    t.integer('user_id').references('users.id').index();
    t.integer('channel_id').references('channels.id').index();

    // Messages
    t.string('subtype', 200);
    t.text('text');

    t.timestamps();
  });
};

exports.down = function (knex, BPromise) {
  return knex.schema.dropTable('events');
};
