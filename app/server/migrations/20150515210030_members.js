exports.up = function(knex, BPromise) {
  return knex.schema.createTable('members', function (t) {
    t.integer('channel_id').references('channels.id');
    t.integer('user_id').references('users.id').onDelete('cascade');
  });
};

exports.down = function(knex, BPromise) {
  return knex.schema.dropTable('members');
};
