exports.up = function(knex, BPromise) {
  return knex.schema.createTable('websockets', function (t) {
    t.integer('user_id').references('users.id');
    t.string('url', 255).unique();
    t.primary([ 'user_id', 'url' ]);
    t.timestamps();
  });
};

exports.down = function(knex, BPromise) {
  return knex.schema.dropTable('websockets');
};
