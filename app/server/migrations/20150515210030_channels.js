exports.up = function(knex, BPromise) {
  return knex.schema.createTable('channels', function (t) {
    t.increments('id').primary();
    t.string('name', 200);
    t.bool('is_channel').defaultTo(true);
    t.bool('is_general').defaultTo(false); // default channel
    t.text('topic');
    t.timestamps();
  });
};

exports.down = function(knex, BPromise) {
  return knex.schema.dropTable('channels');
};
