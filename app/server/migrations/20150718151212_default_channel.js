exports.up = function(knex, BPromise) {
  return knex('channels').insert({
      name: 'lobby',
      is_general: true
    });
};

exports.down = function(knex, BPromise) {
  //TODO
  return knex('channels')
    .where('name', 'lobby')
    .del();
};

