exports.up = function(knex, BPromise) {
  return knex.schema.createTable('users', function (t) {
    t.increments('id').primary();
    t.string('name', 200);
    t.string('token', 200).unique().index();
    t.bool('is_admin').defaultTo(false);
    t.timestamps();
  });
};

exports.down = function(knex, BPromise) {
  return knex.schema.dropTable('users');
};
