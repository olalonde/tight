// A temporary place to put resources that need to be implemented
var _ = require('lodash');

function mock (data) {
  return function (req, res, next) {
    return res.json(_.merge({}, data, {
      ok: true
    }));
  };
}

module.exports = {
  'help.issues.list': mock({
    issues: [],
    more_url: 'http://notimplemented'
  }),
  'commands.list': mock({
    commands: {
      "\/shrug": {
        desc: "Appends \u00af\\_(\u30c4)_\/\u00af to your message",
        usage: "your text",
        name: "\/shrug",
        type: "core"
      },
      "\/me": {
        usage: "your message",
        desc: "Displays action text",
        name: "\/me",
        type: "core"
      }
    }
  }),
  'files.list': mock({
    files: [],
    paging:{
      count: 0,
      total: 0,
      page: 0,
      pages: 0
    }
  }),
  'users.setActive': mock({})
};
