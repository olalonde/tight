module.exports = {
  _loadChannel: function (req, res, next) {
    var Channel = req.m.Channel;
    var channelId = req.query.channel;

    Channel.forge({ id: channelId })
      .fetch()
      .then(function (channel) {
        if (!channel) {
          throw new Error('Channel does not exist');
        }
        req.channel = channel;
        next();
      })
      .catch(next);
  },
  _isChannelMember: function (req, res, next) {
    req.channel.isMember(req.user)
      .then(function (isMember) {
        if (!isMember) {
          throw new Error('You are not a member of this channel');
        }
        next();
      })
      .catch(next);
  },
  history: function (req, res, next) {
    //req.body
    //req.user
    req.channel.history()
      .then(function (history) {
        // TODO: reverse history?
        res.json(history.models.reverse());
      })
      .catch(next);
  }
};
