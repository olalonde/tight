// TODO: sanitize user input... should that be
// done as a bookshelf event hook?
module.exports = {
  create: function (req, res, next) {
    // todo: validate input
    //req.body.token = 'test123';
    req.m.User.forge(req.body)
      .create()
      .then(function (user) {
        res.json({ ok: true, user: user });
      })
      .catch(next);
  },
  update: function (req, res, next) {
    var userId = req.params.userId;
    // todo: validate input
    req.m.User.forge({ id: userId })
      .save(req.body, { method: 'update' })
      .then(function (user) {
        res.json({ ok: true, user: user });
      })
      .catch(next);
  },
  destroy: function (req, res, next) {
    var userId = req.params.userId;
    // todo: validate input
    // todo: /part channels so clients
    // know the user left?
    req.m.User
      .forge({ id: userId })
      .fetch()
      .then(function (user) {
        if (!user) {
          throw new Error('User does not exist');
        }
        return user;
      })
      .then(function (user) {
        return user.save({ name: 'deleted', token: null });
      })
      .then(function () {
        res.json({ ok: true });
      })
      .catch(next);
    /*
    req.m.User.forge({ id: userId })
      .destroy()
      .then(function () {
        res.json({ ok: true });
      })
      .catch(next);
    */
  }
};
