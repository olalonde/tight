module.exports = {
  start: function (req, res, next) {
    var user = req.user;
    user.createWebsocket()
      .bind({})
      .then(function (websocket) {
        this.websocket = websocket;
      })
      .then(function () {
        return req.m.User.fetchAll();
      })
      .then(function (users) {
        this.users = users;
      })
      .then(function () {
        return user.load([ 'channels' ]);
      })
      .then(function (channels) {
        this.channels = user.related('channels').models;
      })
      .then(function () {
        res.json({
          ok: true,
          url: this.websocket.get('url'),
          'self': req.user,
          users: this.users.map(function (user) {
            delete user.token;
            return user;
          }),
          // TODO
          team: {
            id: 1,
            name: 'Not Implemented'
          },
          channels: this.channels
        });
      })
      .catch(next);
  }
};
