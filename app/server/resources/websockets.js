var debug = require('debug')('tight:resources:websockets');

module.exports = {
  destroy: function (req, res, next) {
    return req.m.Websocket.forge({
        url: req.params.websocketId
      })
      .destroy()
      .then(function () {
        res.json({ ok: true });
      });
  },
  create: function websocketPost (req, res, next) {
    var ev = req.body;
    var websocket;
    var reply_to = ev.id;
    var url = req.params.websocketId;

    return req.m.Websocket.forge({
        url: url
      })
      .fetch({ withRelated: 'user' })
      .then(function (_websocket) {
        websocket = _websocket;
        if (!websocket) {
          throw new Error('Cannot find websocket ' + url + '.');
        }
        ev.user = websocket.related('user');
        // TODO: allow other types?
        // TODO: validate event is authorized by user
        ev.type = 'message';
      })
      .then(function () {
        console.log(ev);
        return req.m.Event.processEvent(ev);
      })
      .catch(function (err) {
        debug('Error processing event');
        debug(err);
      })
      .then(function () {
        return websocket.send({ ok: true, reply_to: reply_to });
      })
      .catch(function (err) {
        debug('Error sending reply to websocket');
        debug(err);
      })
      .then(function () {
        res.json({ ok: true });
      })
      .catch(next);
  }
};
