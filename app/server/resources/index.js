module.exports = {
  rtm: require('./rtm'),
  websockets: require('./websockets'),
  users: require('./users'),
  channels: require('./channels'),
  wip: require('./wip')
};
