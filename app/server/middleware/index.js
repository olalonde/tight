module.exports = {
  auth: require('./auth'),
  authInternal: require('./authInternal'),
  authz: require('./authz')
};
