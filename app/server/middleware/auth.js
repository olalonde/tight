var errors = require('common-errors');

// TODO: Passport.js based auth?
module.exports = function (req, res, next) {
  var token = req.query.token || req.body.token;
  if (!token) {
    // TODO: generate guest user?
    return next();
  }

  req.m.User.fromToken(token)
    .then(function (user) {
      if (!user) {
        return next(new errors.HttpStatusError(401, 'Invalid token.'));
      }
      req.user = user;
      console.log(req.user);
      next();
    })
    .catch(next);

};
