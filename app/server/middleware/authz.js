var errors = require('common-errors');

module.exports = {
  isLogged: function (req, res, next) {
    if (!req.user) {
      return next(new errors.AuthenticationRequiredError());
    }
    next();
  },
  isRoot: function (req, res, next) {
    if (!req.user.isRoot()) {
      return next(new errors.NotPermittedError());
    }
    return next();
  }
};
