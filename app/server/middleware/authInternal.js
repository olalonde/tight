var errors = require('common-errors');

module.exports = function (req, res, next) {
  var apikey = req.headers['x-api-key'];
  if (!apikey) {
    return next();
  }
  if (apikey === req.app.get('config').apikey) {
    req.user = {
      isRoot: function () { return true; }
    };
  }
  else {
    return next(new errors.HttpStatusError(401, 'Invalid API token.'));
  }
  next();
};
