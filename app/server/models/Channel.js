var BPromise = require('bluebird');

module.exports = function (bookshelf) {

  var Channel = bookshelf.Model.extend({
    tableName: 'channels',
    hasTimestamps: true,
    members: function () {
      return;
    },
    users: function () {
      var User = bookshelf.model('User');
      var Member = bookshelf.model('Member');
      return this.belongsToMany(User, 'members');
    },
    isMember: function (user) {
      var self = this;
      var Member = bookshelf.model('Member');
      return Member.forge({
          channel_id: self.get('id'),
          user_id: user.get('id')
        })
        .fetch()
        .then(function (member) {
          if (!member) return false;
          return true;
        });
    },
    history: function () {
      var self = this;
      var Event = bookshelf.model('Event');
      return Event.query(function (qb) {
          qb.where('channel_id', self.get('id'));
          qb.limit(100);
          qb.orderBy('created_at', 'desc');
        })
        .fetchAll();
    }
  }, {
    general: function () {
      return Channel.forge({ is_general: true });
    }
  });

  bookshelf.model('Channel', Channel);
  return Channel;
};
