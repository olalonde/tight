var BPromise = require('bluebird');
var crypto   = require('crypto');

module.exports = function (bookshelf) {

  var User = bookshelf.Model.extend({
    tableName: 'users',
    hasTimestamps: true,
    constructor: function() {
      bookshelf.Model.apply(this, arguments);
      this.on('creating', function (model, resp, options) {
        var self = this;
        if (!self.get('token'))
          self.set('token', User.generateToken());
      });
      this.on('created', function (model, resp, options) {
        var self = this;
        var Event = bookshelf.model('Event');
        var Channel = bookshelf.model('Channel');
        var t = options.transacting;

        var evInfo = {
          type: 'team_join',
          user: self
        };

        // Dispatch team_join event
        return Event.processEvent(evInfo, t)
          .then(function () {
            // Join default channels
            return Channel.general()
              .fetchAll({ transacting: t })
              .then(function (generalChannels) {
                return BPromise.all(generalChannels.models.map(function (channel) {
                  return self.join(channel, { transacting: t });
                }));
              });
          });
      });
    },
    // TODO: move this logic using events
    create: function () {
      return this.save();
    },
    join: function (channel, opts) {
      var self = this;
      var Member = bookshelf.model('Member');
      // TODO?: member save hook to generate "user joined channel" event
      return Member.forge({
          user_id: self.get('id'),
          channel_id: channel.get('id')
        })
        .save(null, { transacting: opts.transacting });
    },
    websockets: function () {
      var Websocket = bookshelf.model('Websocket');
      return this.hasMany(Websocket);
    },
    createWebsocket: function () {
      var Websocket = bookshelf.model('Websocket');
      return Websocket.forge({ user_id: this.get('id') }).create();
    },
    channels: function () {
      var Channel = bookshelf.model('Channel');
      // TODO: isMember. load member of channel where user_id=...
      return this.belongsToMany(Channel, 'members');
    }
  }, {
    fromToken: function (token) {
      // TODO
      if (!token) return;
      return User.forge({ token: token }).fetch();
    },
    generateToken: function () {
      return crypto.randomBytes(32).toString('hex');
    }
  });

  bookshelf.model('User', User);
  return User;
};
