var BPromise = require('bluebird');

module.exports = function (bookshelf, app) {
  var config = app.get('config');

  var Member = bookshelf.Model.extend({
    tableName: 'members',
    hasTimestamps: false,
    idAttribute: null,
    user: function () {
      return this.belongsTo(bookshelf.model('User'));
    },
    channel: function () {
      return this.belongsTo(bookshelf.model('Channel'));
    }
  }, {});

  bookshelf.model('Member', Member);

  return Member;
};
