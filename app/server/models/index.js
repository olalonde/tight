module.exports = function (bookshelf, app) {
  return {
    User: require('./User')(bookshelf, app),
    Channel: require('./Channel')(bookshelf, app),
    Websocket: require('./Websocket')(bookshelf, app),
    Member: require('./Member')(bookshelf, app),
    Event: require('./Event')(bookshelf, app)
  };
};
