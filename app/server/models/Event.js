var BPromise = require('bluebird');
var debug = require('debug')('tight:models:event');

module.exports = function (bookshelf) {

  var Event = bookshelf.Model.extend({
    tableName: 'events',
    hasTimestamps: true,
    constructor: function() {
      bookshelf.Model.apply(this, arguments);
      this.on('created', function (model, resp, options) {
        model.dispatch(options.transacting);
      });
    },
    _toJSONs: {
      message: function (attrs) {
        attrs.user = attrs.user_id;
        attrs.channel = attrs.channel_id;
        attrs.ts = attrs.created_at.getTime();
        delete attrs.user_id;
        delete attrs.channel_id;
        delete attrs.id;
        delete attrs.created_at;
        delete attrs.updated_at;
        return attrs;
      },
      team_join: function (attrs) {
        attrs.user = this._user.toJSON();
        attrs.ts = attrs.created_at.getTime();
        delete attrs.user_id;
        return attrs;
      }
    },
    toJSON: function () {
      var attrs = bookshelf.Model.prototype.toJSON.apply(this, arguments);
      var toJSON = this._toJSONs[this.get('type')];
      if (!toJSON) return;
      return toJSON.apply(this, [ attrs ]);
    },
    _dispatchers: {
      message: function (t) {
        var self = this;
        var Websocket = bookshelf.model('Websocket');
        var Channel = bookshelf.model('Channel');
        var Member = bookshelf.model('Member');

        if (this.get('channel_id') === null)
          throw new Error('not implemented');

        return Channel
          .forge({ id: this.get('channel_id') })
          .fetch({ withRelated: [ 'users.websockets' ] })
          .then(function (channel) {
            channel.related('users').forEach(function (user) {
              user.related('websockets').forEach(function (websocket) {
                websocket.send(self);
              });
            });
          });
      },
      team_join: function (t) {
        var self = this;
        var User = bookshelf.model('User');

        return User
          .fetchAll({
            withRelated: [ 'websockets' ],
            transacting: t
          })
          .then(function (users) {
            console.log();
            users.forEach(function (user) {
              user.related('websockets').forEach(function (websocket) {
                websocket.send(self);
              });
            });
          });
      }
    },
    // Send event to all connections affected
    dispatch: function (t) {
      var dispatcher = this._dispatchers[this.get('type')];
      if (!dispatcher) return;
      dispatcher.apply(this, [ t ]);
    }
  }, {
    _eventHandlers: {
      message: function (ev, t) {
        // todo: validation
        // FIXME: validation/authorization
        // user is member of channel?
        // channel exists?
        ev = Event.forge({
          type: ev.type,
          user_id: ev.user.get('id'),
          channel_id: ev.channel,
          text: ev.text
        });
        return ev.save(null, { transacting: t });
      },
      team_join: function (evInfo, t) {
        var ev = Event.forge({
          type: evInfo.type,
          user_id: evInfo.user.get('id')
        });
        ev._user = evInfo.user;
        return ev.save(null, { transacting: t });
      }
    },
    processEvent: function (evInfo, t) {
      var handler = this._eventHandlers[evInfo.type];
      if (!handler) return;
      return handler(evInfo, t)
        .catch(function (err) {
          debug('Error processing event');
          debug(evInfo);
          debug(err);
          process.exit();
        });
    }
  });

  bookshelf.model('Event', Event);
  return Event;
};
