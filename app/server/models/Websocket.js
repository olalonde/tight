var BPromise = require('bluebird');
var shortid  = require('shortid');
var errors   = require('common-errors');
var request  = require('superagent');
var _url     = require('url');

module.exports = function (bookshelf, app) {
  var config = app.get('config');

  var Websocket = bookshelf.Model.extend({
    tableName: 'websockets',
    hasTimestamps: true,
    idAttribute: 'url',
    //idAttribute: null,
    user: function () {
      return this.belongsTo(bookshelf.model('User'));
    },
    create: function () {
      var self = this;
      return bookshelf.transaction(function (t) {
          return self._generateUrl(t)
            .then(function (url) {
              self.set('url', url);
              return self.save(null, { method: 'insert', transacting: t });
            });
        })
        .catch(errors.AlreadyInUseError, function (err) {
          // Wow, unlucky we are. The server/shortid pair is already taken.
          // Retry :)
          return self.create();
        })
        .bind({})
        .then(function (websocket) {
          this.websocket = websocket;
        })
        .then(function () {
          return self.send({ type: 'hello' });
        })
        .then(function (res) {
          //console.log(res);
        })
        .then(function () {
          return this.websocket;
        });
    },
    _generateUrl: function (t) {
      // FIXME: pick server that has lowest load (load balance)
      // or balance based on hash(user.get('id'))
      var url = config.gateway.url.href + '/' + shortid.generate();
      return Websocket.forge({ url: url })
        .fetch({ transacting: t })
        .then(function (websocket) {
          if (websocket) {
            throw new errors.AlreadyInUseError('Websocket', 'url');
          }
          return url;
        });
    },
    send: function (message) {
      var self = this;
      return new BPromise(function (resolve, reject) {
        var url = _url.parse(self.get('url'));
        url.protocol = 'http:'; // https?
        url = url.format();
        request
          .post(url)
          .set('X-API-Key', config.apikey)
          .send(message)
          .end(function (err, res) {
            if (err) {
              err = new errors.io.IOError('Could not reach ' + url + ' (' + err.status + ')', err);
              return reject(err);
            }
            resolve(res);
          });
      });
    }
  }, {});

  bookshelf.model('Websocket', Websocket);

  return Websocket;
};
