var path = require('path');
var debug = require('debug')('tight');
var express = require('express');
var http = require('http');
var knexInit = require('knex');
var bookshelfInit = require('bookshelf');
var errors = require('common-errors');
var toURLObject = require('../utils').toURLObject;
var _ = require('lodash');

var routesInit = require('./routes');
var modelsInit = require('./models');

module.exports = function (config) {
  config = _.merge({
    // default properties
  }, config);
  config.gateway.url = toURLObject(config.gateway.url);
  config.server.url = toURLObject(config.server.url);

  var app = express();
  var server = http.createServer(app);

  app.set('config', config);

  // Initialize knex
  var knex = knexInit(config.knex);

  // Initialize bookshelf using knex
  var bookshelf = bookshelfInit(knex);
  bookshelf.plugin('registry');

  // Initialize models using bookshelf
  var models = modelsInit(bookshelf, app);

  // Alias knex, bookshelf and models on req for convenience
  app.use(function (req, res, next) {
    req.knex = knex;
    req.bookshelf = bookshelf;
    req.m = models;
    next();
  });

  // Set up routes
  routesInit(app);

  // Error handling
  app.use(function (err, req, res, next) {
    var httpErr = new errors.HttpStatusError(err, req);
    if (httpErr.statusCode === 500) {
      console.error(httpErr.name);
      console.error(httpErr.message);
      console.error(httpErr.stack);
    }
    res.status(httpErr.statusCode);
    res.json(httpErr);
  });

  return {
    app: app,
    knex: knex,
    bookshelf: bookshelf,
    models: models,
    server: server,
    start: function (cb) {
      server.listen(config.server.port, cb);
    },
    stop: function (cb) {
      server.close(cb);
    }
  };
};
