#!/usr/bin/env node
var repl       = require('repl');
var program    = require('commander');
var serverInit = require('./');
var config     = require('../config');

var server     = serverInit(config);
var helpers    = require('../test/helpers/');
var db         = helpers.db(server.knex, server.models);

function e(msg) {
  console.error(msg);
  process.exit(-1);
}

program
  //.version('0.0.1') //todo: retrieve from package.json
  //.option('-p, --peppers', 'Add peppers')
  //.option('-c, --cheese [type]', 'Add the specified type of cheese [marble]', 'marble')
  .command('start')
  .description('start http server')
  .action(function () {
    server.start(function (err) {
      var address = server.server.address();
      console.log('Server listening on %s:%s', address.address, address.port);
    });
  });

program
  .command('repl')
  .description('start repl with models and app object available')
  .action(function () {
    var replServer = repl.start({
      prompt: 'tight> ',
      input: process.stdin,
      output: process.stdout
    });
    var context = replServer.context;
    context.models = server.models;
  });

program
  .command('seeds')
  .description('clear db and seed it with dummy users / channels')
  .action(function () {
    // TODO: warning in production
    db.preTest(function (err) {
      if (err) return e(err);
      process.exit();
    });
  });

program.parse(process.argv);
