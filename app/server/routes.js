var debug      = require('debug')('tight:routes');
var path       = require('path');
var express    = require('express');
var bodyParser = require('body-parser');
var resources  = require('./resources');
var middleware = require('./middleware');
var cors       = require('cors');

// Convenience method that creates a new Express router
function router() {
  return express.Router({
      mergeParams: true // inherit parent router's params
  });
}

module.exports = function (app) {
  function parseSocketId(req, res, next) {
    req.params.websocketId = req.params[0];
    next();
  }

  var usersRouter = router()
    .use(middleware.authInternal)
    .use(middleware.authz.isLogged)
    .use(middleware.authz.isRoot)
    .post('/', resources.users.create)
    .put('/:userId', resources.users.update)
    .delete('/:userId', resources.users.destroy);

  var wsRouter = router()
    .use(middleware.authInternal)
    .use(middleware.authz.isLogged)
    .use(middleware.authz.isRoot)
    .post('/*', parseSocketId, resources.websockets.create)
    .delete('/*', parseSocketId, resources.websockets.destroy);

  var apiRouter = router()
    .use('/users', usersRouter)
    .use('/websockets', wsRouter)
    .use(middleware.auth)
    .use(middleware.authz.isLogged)
    // TODO: dynamic routing based on RPC call
    // don't list everything here...
    .post('/rtm.start', resources.rtm.start)
    // todo: no GET?
    .get('/rtm.start', resources.rtm.start)
    .get('/channels.history', [
      resources.channels._loadChannel,
      resources.channels._isChannelMember,
      resources.channels.history
    ]);

  // Work in progess methods
  Object.keys(resources.wip).forEach(function (method) {
    apiRouter.get('/' + method, resources.wip[method]);
    apiRouter.post('/' + method, resources.wip[method]);
  });

  app
    .use(cors())
    .use(bodyParser.json())
    .use(bodyParser.urlencoded())
    .use('/api', apiRouter);
};

