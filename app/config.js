var path  = require('path');
var nconf = require('nconf');
var url   = require('url');

// Case insensitive env variables
// https://github.com/indexzero/nconf/issues/155
Object.keys(process.env).forEach(function (key) {
  process.env[key.toLowerCase()] = process.env[key];
});

nconf.env('_');

nconf.defaults({
  gateway: {
    url: 'ws://localhost:3001',
    ws: {
      /**
       * We delete sockets that haven't received a connection within
       * ttl. (15 mins)
       */
      ttl: 15 * 1000 * 60
    },
    port: 3001
  },
  server: {
    url: 'http://localhost:3000/api',
    port: 3000
  },
  db: {
    host: 'localhost',
    port: 5432,
    database: 'tight_test',
    user: 'postgres'
  },
  apikey: 'testingkey'
});

var config = nconf.get();

if (typeof config.db.port === 'object') {
  config.db.port = parseInt(Object.keys(config.db.port)[0]);
}

config.knex = {
  client: 'pg',
  connection: config.db,
  debug: config.db.debug,
  migrations: {
    directory: path.join(__dirname, './server/migrations')
  }
  /* We dont use knex seeds
  seeds: {
    directory: path.join(__dirname, './test/seeds')
  }
  */
};

module.exports = config;
