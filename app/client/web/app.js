var initClient = require('../');
var client;

document.addEventListener('DOMContentLoaded', function (ev) {
  var eles = {
    channel: document.getElementById('channel'),
    input: document.getElementById('input'),
    connect: document.getElementById('connect'),
    token: document.getElementById('token'),
    connectBtn: document.getElementById('connectBtn'),
    container: document.getElementById('container')
  };

  eles.connectBtn.addEventListener('click', function () {
    eles.connect.style.display = 'none';
    eles.container.style.display = 'block';
    var token = eles.token.value;

    client = initClient({
      server: {
        url: 'http://localhost:3000/api'
      },
      token: token
    });

    write('Connecting with token ' + token + '...');

    client.rtm.start(true, function (err, res) {
      if (err) return write(err);
      var users = res.body.users.reduce(function (acc, user) {
        acc[user.id] = user;
        return acc;
      }, {});
      var channels = res.body.channels;
      client.ws.on('msg', function (message) {
        if (message.type === 'message') {
          write(users[message.user].name + ': ' + message.text);
        }
        else if (message.type === 'hello') {
          write('Connected.');
        }
        else {
          console.log(message);
        }
      });

      eles.input.addEventListener('keyup', function (e) {
        if (e.keyCode == 13) {
          // Enter pressed on input text
          eles.input.disabled = true;
          var text = eles.input.value;

          client.ws.send({
            type: 'message',
            channel: channels[0].id,
            text: text
          }, function (/*todo: err*/res) {
            eles.input.disabled = false;
            if (res.ok) {
              eles.input.value = '';
            }
            console.log(res);
            eles.input.focus();
          });
        }
      });
    });
  });

  function write (msg) {
    var t = document.createTextNode(msg);
    var br = document.createElement('br');
    eles.channel.appendChild(t);
    eles.channel.appendChild(br);
  }
});
