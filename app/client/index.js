var superagent  = require('superagent');
var WebSocket   = require('ws');
var merge       = require('merge');
var url         = require('url');

function toURLObject (u) {
  if (typeof u === 'string') {
    u = url.parse(u);
  }
  return u;
}

module.exports = function (config) {
  // TODO: use options module?
  config = merge({
    // default properties
  }, config);
  //config.gateway.url = toURLObject(config.gateway.url);
  config.server.url = toURLObject(config.server.url);

  var url = config.server.url.format();

  function common (req) {
    req.query({ token: config.token });
    req.url = url + req.url;
  }

  var agent = superagent;

  var client = {
    rtm: {
      start: function (startWebsocket, cb) {
        if (typeof startWebsocket === 'function') {
          cb = startWebsocket;
          startWebsocket = false;
        }
        agent
          .get('/rtm.start')
          .use(common)
          .end(function (err, res) {
            if (!startWebsocket) return cb(err, res);
            if (err) return cb(err);
            // Start websocket
            var ws = new WebSocket(res.body.url);
            // browserify hack
            if (!ws.on) {
              ws.on = function (type, fn) {
                ws.addEventListener(type, function (ev) {
                  return fn(ev.data);
                });
              };
            }
            // browserify hack
            if (!ws.emit) {
              ws.emit = function (type, data) {
                var ev = new Event(type);
                ev.data = data;
                ws.dispatchEvent(ev);
              };
            }

            var id = 0;
            var requests = {};

            // Simulate request/response protocol
            ws.on('message', function (message) {
              // Oops, ws and browser WebSocket are slightly
              // different here.
              var msg = message;
              if (typeof message === 'string') {
                try {
                  msg = JSON.parse(message);
                }
                catch (e) {
                  throw Error('Could not parse message ' + message);
                }
              }
              if (msg.reply_to && requests[msg.reply_to]) {
                requests[msg.reply_to].cb(msg);
                //delete requests[msg.reply_to]
              }
              ws.emit('msg', msg);
            });

            ws.sendOrig = ws.send;
            ws.send = function (msg, cb) {
              // To correlate messages
              msg.id = ++id;
              if (cb) {
                requests[msg.id] = { msg: msg, cb: cb };
              }
              msgString = JSON.stringify(msg) + '\n';
              return ws.sendOrig(msgString);
            };

            client.ws = ws;

            // Send pings automatically
            // TODO: use _.debounce?
            setInterval(function () {
              ws.send({ type: 'ping' });
            }, 10000);


            return cb(null, res);
          });
      }
    },
    channels: {
      history: function (params, cb) {
        agent
          .get('/channels.history')
          .query(params)
          .use(common)
          .end(function (err, res) {
            if (err) return cb(err);
            return cb(null, res.body);
          });
      }
    }
  };

  return client;
};
