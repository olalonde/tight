var knex = require('./app/config').knex;

module.exports = {
  development: knex,
  production: knex
};
