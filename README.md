# Development

## Install

Create `.env` file with:

```
DB_HOST=docker
DB_USER=postgres
DB_PORT=5432
DB_DATABASE=tight_test
DEBUG=*,-pool2,-knex,-mocha:*,express*,superagent*
```

```bash
docker-compose up createdbs
npm install
foreman run ./node_modules/.bin/knex migrate:latest
```

## Run

```bash
docker-compose up -d services
# foreman run npm test
nodemon --exec foreman start
```

## TODO

Dont use X-API-Key auth. Each user should have api key / secret and
services that integrate should use an admin's api key / secret.
